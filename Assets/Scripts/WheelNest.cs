using UnityEngine;
using System.Collections;

public class WheelNest : MonoBehaviour {
	
	// This script puts the driving wheels in their right places during initialisation
	// (09/010/16 - What did I mean by the right places?? Zero vectors?)
	public Vector3 Lpos,Lrot,Lscale, Rpos, Rrot,Rscale;
	public GameObject xLeftWheels, xRightWheels;
	public bool update = false;
	// Seems like these variables just exist to reflect the train velocity in the inspector
	public float speedR, speedL;
	private CarProperties m_xLocomotiveProperties;
	
	private AnimationState m_xLeftWheelsAnimState, m_xRightWheelsAnimState;
	
	void Start () 
	{
		m_xLeftWheelsAnimState = xLeftWheels.GetComponent<Animation>().GetComponent<Animation>()["ArmatureAction_001"];
		m_xRightWheelsAnimState = xRightWheels.GetComponent<Animation>().GetComponent<Animation>()["ArmatureAction_001"];
		
		m_xLocomotiveProperties = transform.root.GetComponent<CarProperties>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Presuming this is done in Update because it's post initialisation
		// Qu: Is there a late initialisation method for MonoBehaviour classes?
		if(update)
		{
			xLeftWheels.transform.localPosition = Lpos;
			xLeftWheels.transform.localRotation = Quaternion.Euler(Lrot);
			xLeftWheels.transform.localScale = Lscale;
	
	
			xRightWheels.transform.localPosition = Rpos;
			xRightWheels.transform.localRotation = Quaternion.Euler(Rrot);
			xRightWheels.transform.localScale = Rscale;
	
			update = false;
		}
		
		m_xRightWheelsAnimState.speed = speedR = 3.0f*m_xLocomotiveProperties.m_xVelocity.x;
		m_xLeftWheelsAnimState.speed = speedL = 3.0f*m_xLocomotiveProperties.m_xVelocity.x;
	}
}
