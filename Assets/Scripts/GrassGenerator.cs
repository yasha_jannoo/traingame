﻿using UnityEngine;
using System.Collections;

public class GrassGenerator : MonoBehaviour {
	
	public GameObject m_xCameraObjectReference;
	public GameObject m_xGrassObject;
	
	public float m_fGridDimension;
	private const uint uNUMGRASSOBJECTSPERSQR = 50;
	private GameObject[] m_xGrassObjects;
	private int iLevelGrassSeed = 28020;
	private int m_iWidth = 10;
	private int m_iHeight = 10;
	
	// Use this for initialization
	void Start () 
	{
		// Set the same (arbitrary) seed for each track. 
		// TODO: Make this seed from the track name or something so that it's unique.
		Random.seed = iLevelGrassSeed;
		
		// 9 squares
		m_xGrassObjects = new GameObject[16*uNUMGRASSOBJECTSPERSQR];
		
		// TODO: This is not how to check for nullity in C#.
		if(!m_xGrassObject)
		{
			return;
		}
		// Create uNUMGRASSOBJECTS*number of squares gameobjects
		for( uint u = 0; u < 16*uNUMGRASSOBJECTSPERSQR; u++ )
		{
			m_xGrassObjects[u] = (GameObject)GameObject.Instantiate(m_xGrassObject, new Vector3(0.0f,0.0f,0.0f), Quaternion.identity);
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{	
		int iNumTrees = 16*((int)uNUMGRASSOBJECTSPERSQR);
		for(int u = 0; u < 4; u++)
		{
			for(int w = 0; w < 4; w++)
			{
				float fShiftX = m_fGridDimension*(u - 2);
				float fShiftY = m_fGridDimension*(w - 2);
				// Find the index of this grid square added to the camera centre
				int iGridIndex = u + 3*w;
				// Set the seed to be this index 
				Random.seed = iGridIndex + iLevelGrassSeed;
				
				for(int i = 0; i < uNUMGRASSOBJECTSPERSQR; i++)
				{				
					Vector3 xGeneratedPosition = new Vector3( 
						Random.Range(0.0f, m_fGridDimension) + fShiftX, 
						0.0f, 
						Random.Range(0.0f, m_fGridDimension) + fShiftY);
					
					if(iNumTrees > 0)
					{
						m_xGrassObjects[iNumTrees - 1].transform.position = xGeneratedPosition;
						iNumTrees--;
					}
					
				}
			}
		}
	}
}
