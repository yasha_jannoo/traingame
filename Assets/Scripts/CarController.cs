using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI; // Required when Using UI elements.
using System.Collections.Generic;

/* what's the important information for each train car?
* each car must have location data
* they must have type data
* and space for other gameplay-influenced data, etc damage and crew
* 
* location data comes in bits: the trackID which says which piece of
* track the car is on, the parameter which decides where on the track
* it is, the velocity vector, the position vector, the euler angles
* 
* type data describes what sort of train car we have. this influences
* the model drawn, and the collision body.
* */
public class TrainCar
{
    public Vector3 pos, v; //t is parameter of tracks, v is velocity
    public float t;
    public float angle; //angle is a cheap way of finding the direction the car points in (change it to four wheels!)
    public Quaternion rotation;

    public TrainCar(float zt, Vector3 zv)
    {
        t = zt;
        v = zv;
    }
}

public class CarController : MonoBehaviour
{
    private TrackData m_xTrackData;
    private Slider m_xProgressSlider;
    private Slider m_xLocoForceSlider;
    public float time; //time is time, n is dummy, length is length for that piece of track
    public GameObject m_xEngineObj;
    public GameObject m_xTenderObj;
    public bool m_bRequestAddCar = false;

    public List<GameObject> carriage_objects = new List<GameObject>();
    public List<CarProperties> carriage_properties = new List<CarProperties>();

    void AddCarriage(float zt, Vector3 zVel, GameObject type)
    {
        GameObject xNewCarriage = Instantiate(type);
        xNewCarriage.transform.parent = transform;
        carriage_objects.Add(xNewCarriage);
        carriage_properties.Add(carriage_objects[carriage_objects.Count - 1].GetComponent<CarProperties>());
        carriage_properties[carriage_objects.Count - 1].Initialise(zt, zVel);
    }

    // Use this for initialization
    void Start()
    {
        AddCarriage(25.0f, new Vector3(10.0f, 0.0f, 0.0f), m_xEngineObj);

        time = 0;

        m_xTrackData = GameObject.Find("Tracks").GetComponent<TrackData>();
        Assert.IsTrue(m_xTrackData != null, "Cannot find tracks object or data");

        m_xProgressSlider = GameObject.Find("Town Progress").GetComponent<Slider>();
        Assert.IsTrue(m_xProgressSlider != null, "Cannot find progress slider");

        m_xLocoForceSlider = GameObject.Find("Loco Force Slider").GetComponent<Slider>();
        Assert.IsTrue(m_xLocoForceSlider != null, "Cannot find loco force slider");
    }

    private float GetTrainProgress()
    {
        return carriage_properties[0].t / m_xTrackData.m_xTrackList[0].m_fTotalLength;
    }

    public void SetTrainForceBySlider(float fValue)
    {
        for (int j = 0; j < carriage_properties.Count; j++)
        {
            carriage_properties[j].m_fForwardThrust = m_xLocoForceSlider.value;
        }
    }

    void FixedUpdate()
    {
        if (m_bRequestAddCar)
        {
            CarProperties carprop = carriage_properties[carriage_properties.Count - 1];
            AddCarriage(carprop.t - carprop.m_fAttatchPointDistance, carprop.m_xVelocity, m_xTenderObj);
            m_bRequestAddCar = false;
        }

        for (int j = 0; j < carriage_properties.Count; j++)
        {
            // TODO: RK4 will later acceppt a force parameter, and time should be updated outside of it!
            RK4Solve.PropagateForwards(ref carriage_properties[j].t, ref carriage_properties[j].m_xVelocity.x, ref time, carriage_properties[j].m_fForwardThrust);

            // Lerp vec is the world-space position of the cat track given how far along the parameter t it is.
            // Current vector is the difference in the position of the next and current track piece centres.
            Vector3 lerpVec;
            Vector3 currVec;
            ParamToVector(carriage_properties[j].t, m_xTrackData.m_xTrackList[0], ref carriage_properties[j].m_iCurrentTrackPieceIndex, out currVec, out lerpVec);

            carriage_properties[j].m_xWorldSpacePosition = lerpVec;
            carriage_properties[j].angle = (180f / Mathf.PI) * Mathf.Atan2(currVec.z, currVec.x);
            carriage_properties[j].rotation = Quaternion.Euler(new Vector3(0f, 180f - carriage_properties[j].angle, 90f));
            carriage_properties[j].m_xCurrentTrack = m_xTrackData.m_xTrackList[0];
        }

        // transform.position should -reflect- the state of the carriage_properties rather than be authoritive.
        for (int i = 0; i < carriage_objects.Count; i++)
        {
            carriage_objects[i].transform.position = carriage_properties[i].m_xWorldSpacePosition;
            carriage_objects[i].transform.rotation = carriage_properties[i].rotation;
        }

        // Pretend that t is the progress between towns for now
        m_xProgressSlider.value = GetTrainProgress();
    }

    void ParamToVector(float t, Tracks curve, ref int ztrackcount, out Vector3 xCurrentVector, out Vector3 xLerpVector)
    {
        xCurrentVector = Vector3.zero;
        xLerpVector = Vector3.zero;
        // Slow but works, this is the error correction
        for (int i = 0; i < curve.m_axTrackConnectingPoint.Count; i++)
        {
            if (t >= curve.m_fLength[i] && t < curve.m_fLength[i + 1])
            {
                int n = i;
                ztrackcount = i;
                xCurrentVector = curve.m_axTrackConnectingPoint[n + 1] - curve.m_axTrackConnectingPoint[n];
                float percentage = 1 - ((t - curve.m_fLength[n]) / xCurrentVector.magnitude);
                xLerpVector = Vector3.Lerp(curve.m_axTrackConnectingPoint[n + 1], curve.m_axTrackConnectingPoint[n], percentage);
            }
        }
    }
}