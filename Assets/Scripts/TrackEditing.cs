using UnityEngine;
using System.Collections.Generic;

public class TrackEditing : MonoBehaviour
{
    private const float fMaximumAngle = (Mathf.PI / 90);
    private float m_fAlpha0;
    private float m_fAlpha1;
    private float m_fAlpha2;
    private float m_fAlpha3;
    public bool m_bShouldLayTrack = false;
    public bool m_bShouldMakeFork = false;

    public GameObject selectionBall;
    public List<GameObject> clonedselection;

    public int selectedTrack;
    private Vector3 initialtrack;

    // TODO: Do I do need lists of meshes at any point for LOD behaviour.
    private float m_fGauge;
    public bool m_bRecalcMesh { get; set; }
    //public List<Mesh> trackmeshes; will i ever need to make several meshes/submeshes?
    public Material m_xTrackMaterial;
    private List<Tracks> m_axTrackList;

    public void TrackClickAdd()
    {
        m_axTrackList = transform.GetComponent<TrackData>().m_xTrackList;

        if (m_bShouldLayTrack)
        {
            //Lines 1, 2, 3. On when tracklaying is active.
            Lines123(fMaximumAngle);

            m_bRecalcMesh = true;

            m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Add(new Vector3(10f * Mathf.Cos(m_fAlpha0 + m_fAlpha1), 0f, 10f * Mathf.Sin(m_fAlpha0 + m_fAlpha1)) + m_axTrackList[selectedTrack].m_axTrackConnectingPoint[m_axTrackList[selectedTrack].edit_point]);
            m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Add(new Vector3(10f * Mathf.Cos(m_fAlpha0 + m_fAlpha1 + m_fAlpha2), 0f, 10f * Mathf.Sin(m_fAlpha0 + m_fAlpha1 + m_fAlpha2)) + m_axTrackList[selectedTrack].m_axTrackConnectingPoint[m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Count - 1]);
            m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Add(new Vector3(10f * Mathf.Cos(m_fAlpha0 + m_fAlpha1 + m_fAlpha2 + m_fAlpha3), 0f, 10f * Mathf.Sin(m_fAlpha0 + m_fAlpha1 + m_fAlpha2 + m_fAlpha3)) + m_axTrackList[selectedTrack].m_axTrackConnectingPoint[m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Count - 1]);

            m_axTrackList[selectedTrack].edit_point = m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Count - 1;

            int x = m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Count;
            m_fAlpha0 = Mathf.Atan2(m_axTrackList[selectedTrack].m_axTrackConnectingPoint[x - 1].z - m_axTrackList[selectedTrack].m_axTrackConnectingPoint[x - 2].z, m_axTrackList[selectedTrack].m_axTrackConnectingPoint[x - 1].x - m_axTrackList[selectedTrack].m_axTrackConnectingPoint[x - 2].x);

            m_axTrackList[selectedTrack].CalculateTotalTrackLength();
        }

        m_bShouldMakeFork = false;
    }


    void Lines123(float maxangle)
    {
        float mouse_x = (Input.mousePosition.x - Screen.width / 2) * (2 * Mathf.Cos(maxangle) / Screen.width);
        float mouse_y = (Input.mousePosition.y - Screen.height / 2) * (2 * Mathf.Sin(maxangle) / Screen.height);
        float beta = Mathf.Atan2(mouse_y, mouse_x);
        float cosmaxangle = Mathf.Cos(maxangle);

        if (Mathf.Cos(beta - 0) >= cosmaxangle)
        { m_fAlpha1 = beta; }
        if (Mathf.Cos(beta - 0) < cosmaxangle && beta > 0)
        { m_fAlpha1 = maxangle; }
        if (Mathf.Cos(beta - 0) < cosmaxangle && beta < 0)
        { m_fAlpha1 = -maxangle; }

        if (Mathf.Cos(beta - m_fAlpha1) >= cosmaxangle)
        { m_fAlpha2 = beta; }
        if (Mathf.Cos(beta - m_fAlpha1) < cosmaxangle && beta > 0)
        { m_fAlpha2 = maxangle; }
        if (Mathf.Cos(beta - m_fAlpha1) < cosmaxangle && beta < 0)
        { m_fAlpha2 = -maxangle; }

        if (Mathf.Cos(beta - m_fAlpha2) >= cosmaxangle)
        { m_fAlpha3 = beta; }
        if (Mathf.Cos(beta - m_fAlpha2) < cosmaxangle && beta > 0)
        { m_fAlpha3 = maxangle; }
        if (Mathf.Cos(beta - m_fAlpha2) < cosmaxangle && beta < 0)
        { m_fAlpha3 = -maxangle; }
    }

    // Use this for initialization
    void Start()
    {
        clonedselection = new List<GameObject>();
        m_bShouldLayTrack = true;
        initialtrack = Vector3.zero;

        m_fGauge = 2f;
        m_bRecalcMesh = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_bRecalcMesh)
        {
            m_axTrackList = transform.GetComponent<TrackData>().m_xTrackList;
            Mesh xMesh = GetComponent<MeshFilter>().mesh;
            MeshCalc(ref xMesh, m_axTrackList);
        }

        if (!m_bShouldLayTrack)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit rayhit;
            Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayhit);

            if (rayhit.collider != null)
            {
                // Remove selection objects
                for (int i = 0; i < clonedselection.Count; i++)
                {
                    Destroy(clonedselection[i]);
                }

                selectionBallproperties ballhitprops = rayhit.transform.gameObject.GetComponent<selectionBallproperties>();

                if (ballhitprops.type == "Split")
                {
                    /*access the script of the selection ball to get at its properties more easily later on (HOW?)
					object scriptball = rayhit.transform.gameObject.GetComponent<selectionBallproperties>();
					also why can't I make this into a method somewhere else?
					set the vector for the position of the new splitting track as the vector of the segment of the track we split at*/
                    initialtrack = m_axTrackList[ballhitprops.tracknumber].m_axTrackConnectingPoint[ballhitprops.editpoint];
                    m_bShouldMakeFork = true;
                    m_axTrackList.Add(new Tracks(initialtrack, ballhitprops.tracknumber));

                    //setting the correct active track so we can add to the new fork
                    selectedTrack = m_axTrackList.Count - 1;
                    Debug.Log(selectedTrack);

                    //reminding the last track that this change has occured and tell it where
                    //tracklist[ballhitprops.tracknumber].joins.Add(new Intersection (activetrack, ballhitprops.editpoint));
                    //adding a point to the new track, the first point where it is splitting from the last track
                    m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Add(initialtrack);
                    m_fAlpha0 = ballhitprops.angle;
                    m_bShouldLayTrack = true;
                }

                if (ballhitprops.type == "Join")
                {

                    Vector3 virtualjoin = // - vector at end of joining line + vector at chosen ball. vector quantity. 
                            m_axTrackList[0].m_axTrackConnectingPoint[ballhitprops.editpoint] - m_axTrackList[selectedTrack].m_axTrackConnectingPoint[m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Count - 1];

                    float scale = Mathf.Sqrt((100f - 0.25f * (virtualjoin.sqrMagnitude)));

                    Vector3 normal = new Vector3(0, 1, 0);
                    Vector3 perpendicular = Vector3.Cross(normal, virtualjoin);

                    perpendicular.Normalize();

                    m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Add(m_axTrackList[selectedTrack].m_axTrackConnectingPoint[m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Count - 1] + virtualjoin / 2 - perpendicular * scale);
                    m_axTrackList[selectedTrack].m_axTrackConnectingPoint.Add(m_axTrackList[0].m_axTrackConnectingPoint[ballhitprops.editpoint]);
                    Debug.Log(scale);
                    Debug.Log(perpendicular.magnitude);
                    m_bShouldLayTrack = true;
                }
            }
        }
    }

    // Called when the Mesh needs recalculating
    private void MeshCalc(ref Mesh xMesh, List<Tracks> axTrackList)
    {
        xMesh.Clear();
        
        // If there's no data in the list then warn the user, but this might just be a Clear command
        if (axTrackList.Count == 0)
        {
            Debug.LogWarning("No tracks being drawn, should only happen on Clear");
            return;
        }
                
        List<Vector3> axTracksToDraw = new List<Vector3>();
        axTracksToDraw.Clear();

        List<Vector2> v2UV = new List<Vector2>();
        List<Vector3> v3Vertices = new List<Vector3>();
        List<int> iTriangles = new List<int>();


        int[] abDrawEnds = new int[2];
        abDrawEnds[0] = 0;

        // Add a drawing stop point each time it hits a tracklist.cp_pos dead-end or specified render LOD
        if (axTrackList.Count != 0)
        {
            abDrawEnds[1] = axTrackList[0].m_axTrackConnectingPoint.Count;
        }
        // Assign stored tracks to drawn tracks. TODO: This needs to become LOD behaviour.
        for (int i = 0; i < axTrackList.Count; i++)
        {
            if (axTrackList[i].m_axTrackConnectingPoint.Count != 0)
            {
                for (int k = 0; k < axTrackList[i].m_axTrackConnectingPoint.Count; k++)
                {
                    axTracksToDraw.Add(axTrackList[i].m_axTrackConnectingPoint[k]);
                }
            }
        }


        v3Vertices.Clear();
        v2UV.Clear();

        // This loop should add four vertices
        for (int i = 1; i < axTracksToDraw.Count; i++)
        {
            float drawangle = Mathf.Atan2((axTracksToDraw[i] - axTracksToDraw[i - 1]).z, (axTracksToDraw[i] - axTracksToDraw[i - 1]).x);

            float rsin = Mathf.Sin(drawangle);
            rsin = m_fGauge * rsin;

            float rcos = Mathf.Cos(drawangle);
            rcos = m_fGauge * rcos;
            //This is making points on either side of the trackstodraw point "i".

            v3Vertices.Add(new Vector3(rsin, 0f, -rcos) + axTracksToDraw[i - 1]);
            v2UV.Add(new Vector2(0, 0));
            v3Vertices.Add(new Vector3(-rsin, 0f, rcos) + axTracksToDraw[i - 1]);
            v2UV.Add(new Vector2(0, 1));
            v3Vertices.Add(new Vector3(rsin, 0f, -rcos) + axTracksToDraw[i]);
            v2UV.Add(new Vector2(1, 0));
            v3Vertices.Add(new Vector3(-rsin, 0f, rcos) + axTracksToDraw[i]);
            v2UV.Add(new Vector2(1, 1));
        }
        // TODO: Are more vertices needed?

        iTriangles.Clear();
        //below needs to now work for when i'm drawing different numbers of tracks

        /* What is the index label for each vertex of track?
		 * GUESS: this is given by, (I think 1 and 2 are swapped.. or the diagonal is bottom right to top left)
		 *   ______
		 * 2|     /|3
		 *  |    / |
		 *  |   /  |
		 *  |  /   |
		 *  | /    |
		 * 0|/_____|1
		 *
		 * */

        for (int j0 = 0; j0 < 4 * (axTrackList[0].m_axTrackConnectingPoint.Count - 1); j0 += 4)
        {
            iTriangles.Add(j0 + 1);
            iTriangles.Add(j0 + 2);
            iTriangles.Add(j0);

            iTriangles.Add(j0 + 3);
            iTriangles.Add(j0 + 2);
            iTriangles.Add(j0 + 1);
        }


        xMesh.vertices = v3Vertices.ToArray();
        xMesh.uv = v2UV.ToArray();
        xMesh.triangles = iTriangles.ToArray();
        xMesh.RecalculateNormals();
        GetComponent<MeshRenderer>().material = m_xTrackMaterial;

        m_bRecalcMesh = false;

    }

}