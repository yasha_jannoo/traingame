using UnityEngine;
using UnityEngine.Assertions;

public class CameraBoxScript : MonoBehaviour
{

    public enum CameraMode
    {
        CAMERA_EDIT,
        CAMERA_FOLLOW
    };

    // These two should probably not be public. 
    // mainCam should be set in Update as the main camera being used.
    // mainCamBox should be found as the gameObject that this script is attached to(?)
    public GameObject mainCam;
    public GameObject mainCamBox;


    public static CameraMode s_eRequestedCameraMode;
    [HideInInspector]
    public static CameraMode s_eCurrentCameraMode;
    public Vector3 locoCamRotVec;

    private Transform tempTrans;
    private CarController m_xTrainCarController;
    private TrackEditing m_xTrackEditor;
    private Vector3 m_xInitialMouseOffset;
    private Vector3 initCamBox;
    private Vector3 locoCamAdd;



    //----------------------------------------------------------------------------------------------	

    //----------------------------------------------------------------------------------------------	

    // Use this for initialization
    void Start()
    {

        s_eRequestedCameraMode = CameraMode.CAMERA_EDIT;
        // Must be different to m_eRequestedCameraMode to force logic in first Update.
        s_eCurrentCameraMode = CameraMode.CAMERA_FOLLOW;
        Assert.IsTrue(s_eCurrentCameraMode != s_eRequestedCameraMode);

        m_xTrainCarController = GameObject.Find("Train").GetComponent<CarController>();
        m_xTrackEditor = GameObject.Find("Tracks").GetComponent<TrackEditing>();
        locoCamRotVec = new Vector3(17f, 90f, 0f);
    }

    public void RequestEdit()
    {
        s_eRequestedCameraMode = CameraMode.CAMERA_EDIT;
    }

    public void RequestPlay()
    {
        s_eRequestedCameraMode = CameraMode.CAMERA_FOLLOW;
    }

    // Update is called once per frame
    void Update()
    {

        if (m_xTrainCarController.carriage_objects.Count > 0)
        {
            tempTrans = m_xTrainCarController.carriage_objects[0].transform;
        }

        if (s_eCurrentCameraMode != s_eRequestedCameraMode)
        {
            switch (s_eRequestedCameraMode)
            {
                case CameraMode.CAMERA_EDIT:
                    {

                        mainCam.GetComponent<Camera>().orthographic = true;
                        Vector3 temp;

                        mainCamBox.transform.parent = null;

                        temp = mainCamBox.transform.position;
                        mainCamBox.transform.localPosition = new Vector3(temp.x, 0.0f, temp.z);
                        mainCamBox.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 0.0f));

                        mainCam.transform.localPosition = new Vector3(0.0f, 125f, 0.0f);
                        break;
                    }
                case CameraMode.CAMERA_FOLLOW:
                    {
                        mainCam.GetComponent<Camera>().orthographic = false;

                        mainCamBox.transform.parent = tempTrans;

                        mainCamBox.transform.localPosition = new Vector3(7f, 0.0f, 0.0f);
                        //mainCamBox.transform.localRotation *= Quaternion.Euler(new Vector3(0.0f,37f,0.0f))*Quaternion.Euler(testVec);
                        mainCamBox.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, locoCamRotVec.y, 0.0f)) * Quaternion.Euler(new Vector3(0.0f, 0.0f, locoCamRotVec.z)) * Quaternion.Euler(new Vector3(locoCamRotVec.x, 0.0f, 0.0f));

                        //mainCamBox.transform.localRotation = (Quaternion.Euler(new Vector3(0.0f,37f,0.0f)))*mainCamBox.transform.localRotation;

                        mainCam.transform.localPosition = new Vector3(0.0f, 25f, 0.0f);
                        //mainCam.transform.localRotation = Quaternion.Euler(new Vector3(90f,0.0f,0.0f))*Quaternion.Inverse(mainCamBox.transform.localRotation);
                        break;
                    }
            }

            s_eCurrentCameraMode = s_eRequestedCameraMode;
        }

        switch (s_eCurrentCameraMode)
        {
            case (CameraMode.CAMERA_EDIT):
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        // Debug.Log("Pressed left click.");
                        if (m_xTrackEditor.m_bShouldLayTrack) { m_xTrackEditor.TrackClickAdd(); }
                    }
                    if (Input.GetMouseButton(1))
                    {
                        Vector3 xMouseOffset;
                        // Debug.Log("Holding right click.");
                        xMouseOffset = Input.mousePosition - m_xInitialMouseOffset;
                        xMouseOffset = new Vector3(-0.1f * xMouseOffset.x, 0.0f, -0.1f * xMouseOffset.y);
                        transform.localPosition = initCamBox + xMouseOffset;
                        // print (Input.mousePosition - initMouseVec);
                    }
                    else
                    {
                        m_xInitialMouseOffset = Input.mousePosition;
                        initCamBox = transform.localPosition;
                    }
                }
                break;
            case (CameraMode.CAMERA_FOLLOW):
                {
                    if (Input.GetMouseButton(1))
                    {
                        Vector3 differen;
                        //Debug.Log("Holding right click.");
                        differen = Input.mousePosition - (new Vector3((float)Screen.width / 2f, (float)Screen.height / 2f, 0.0f));
                        differen = new Vector3(-0.01f * differen.y, 0.0f, 0.01f * differen.x);
                        locoCamAdd = differen;
                        mainCamBox.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, locoCamRotVec.y, 0.0f)) * Quaternion.Euler(new Vector3(0.0f, 0.0f, locoCamRotVec.z)) * Quaternion.Euler(new Vector3(locoCamRotVec.x, 0.0f, 0.0f));
                    }
                }
                break;
        }
    }



    void FixedUpdate()
    {
        {
            locoCamRotVec = locoCamRotVec + locoCamAdd;
            locoCamAdd = Vector3.zero;
        }
    }


}

/* I have a locomotive camera problem that I don't understand to do with the quaternions.
 * I have some fixes thought up already, but I'd like to return to this and work out 
 * exactly what is going wrong. Why is it difficult to apply a rotation about two axes?
 * 
 * 1: construct a quaternion that is a result of three rotation quaternions
 * 2: make code that creates a camerabox -> empty -> maincam hierarchy
 * 3: just find the equation of a circle and change the camera position, then use a lookat on it.
 * 
 * for now I will pick 3, but I will also attempt the others later on. After that I need to work out why what I'm doing is broken.
 * The * operator combines quaternions from left to right....
 * 
 * I still can't implement 3. I am going to try to make it so I only need to rotate in two axes?
 */