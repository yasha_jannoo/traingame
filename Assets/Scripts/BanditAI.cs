using UnityEngine;
using System.Collections;

public class BanditAI : MonoBehaviour
{
	
	private CarProperties xTargetProperties;
	public bool attack;
	
	// Use this for initialization
	void Start ()
    {
        xTargetProperties = GameObject.Find("Train").GetComponent<CarController>().carriage_properties[0];
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (attack)
		{
            
            Vector3 xOwnPosition = gameObject.transform.position;
            Vector3 attackVector = FindAttackVector(xOwnPosition, 5.0f, xTargetProperties);
            gameObject.transform.position = xOwnPosition + 0.5f * (attackVector - xOwnPosition).normalized;
		}
	}
	
	Vector3 FindAttackVector ( Vector3 zbanditPos, float banditMaxV, CarProperties trainprops )
	{
        float fCarSpeedSqred = trainprops.m_xVelocity.x;
        fCarSpeedSqred *= fCarSpeedSqred;
        Tracks curve = trainprops.m_xCurrentTrack;

        for (int i = trainprops.m_iCurrentTrackPieceIndex; i < curve.m_axTrackConnectingPoint.Count; i++)
		{
			Debug.Log (trainprops.m_iCurrentTrackPieceIndex);
			Debug.Log (curve.m_axTrackConnectingPoint.Count);
			float banditTime = (curve.m_axTrackConnectingPoint[i] - zbanditPos).magnitude/banditMaxV;
			if(banditTime < (curve.m_fLength[i]-curve.m_fLength[trainprops.m_iCurrentTrackPieceIndex])/trainprops.m_xVelocity.x)
				{
					return curve.m_axTrackConnectingPoint[i];
				}
		}			
		Debug.Log("No Attack Vector Found");
		return Vector3.zero;
	}
}