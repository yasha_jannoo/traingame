using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TreeGen : MonoBehaviour {
	
	public GameObject part1, part2, part3;
	public List<GameObject> trees;
	
	private float pi180, testagainst;
	
	private struct Tree
	{
		public GameObject tree;
		public GameObject p1,p2,p3;
		
		public Tree (GameObject zp1, GameObject zp2, GameObject zp3)
		{
			tree = new GameObject();
			tree.name = "firstree";
			p1 = (GameObject)Instantiate(zp1);
			p1.transform.parent = tree.transform;
			p2 = (GameObject)Instantiate(zp2);
			p2.transform.parent = tree.transform;
			p3 = (GameObject)Instantiate(zp3);
			p3.transform.parent = tree.transform;
			
		}
		
	}
	
	
	
	// Use this for initialization
	void Start () {
	
		
	
		pi180 = Mathf.PI/180f;
		// WARNING LOOK AT THIS, this is only testing treegen anyway
		pi180 = 10f*pi180;
		
		
		trees = new List<GameObject>();
	
		for (int i = 0; i < 100; i++)
			{
				float fi = (float)i;
				
				trees.Add((GameObject)Instantiate(
													(new Tree(part1,part2,part3)).tree,
													new Vector3(10f*fi,0.3f*i*Mathf.Sin(pi180*fi),0f),
													Quaternion.Euler(Vector3.zero))
						 );
			}
		
		if(trees.Count > 0)
			{
				testagainst = trees[0].transform.position.y;
				
				for (int i = 0; i<100; i++)
					{
							if(trees[i].transform.position.y < testagainst - 7f)
							{
								trees[i].transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
								trees[i].transform.GetChild(2).GetComponent<MeshRenderer>().enabled = false;
							}
							else if (trees[i].transform.position.y < testagainst - 3f)
							{
								trees[i].transform.GetChild(2).GetComponent<MeshRenderer>().enabled = false;
							}
					}
			}
	}
}
