using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SmokeEmitter : MonoBehaviour
{
    // This class loads sounds from a specific directory then plays them periodically in sequence.
    public const int m_iSpawnPeriod = 80;
    public float m_fTimeMultiplier = 15.0f;
    private int m_iFramesSinceLastSpawn = 0;
    private int m_iSoundIndex = 0;
    // Not sure what this is.
    public GameObject smokeparent;
    public GameObject smokelett;

    // Properties of this "SmokeEmitter" object
    private Vector3 m_xPrevPosition;
    private Vector3 m_xVelocity;

    private AudioSource xAudioSourceComponent;
    public AudioClip[] xChimneySounds;

    // Use this for initialization
    void Start()
    {
        smokeparent = Instantiate(smokeparent);
        xAudioSourceComponent = transform.GetComponent<AudioSource>();
        xChimneySounds = LoadSoundsFromAssets("Sounds/Loco/Exhaust");
        m_xPrevPosition = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (m_iFramesSinceLastSpawn > m_iSpawnPeriod)
        {
            GameObject xTempReference;
            xTempReference = Instantiate(smokelett, transform.position, Quaternion.Euler(Vector3.zero));
            xTempReference.transform.parent = smokeparent.transform;
            // Provide an initial velocity to this smoke element
            xTempReference.GetComponent<SmokeMover>().m_xSmoke += m_xVelocity;
            xTempReference.GetComponent<SmokeMover>().m_fTimeMultiplier = m_fTimeMultiplier;

            int iNumSounds = xChimneySounds.Length;

            if (iNumSounds != 0)
            {
                m_iSoundIndex = m_iSoundIndex++ % iNumSounds;
                xAudioSourceComponent.clip = xChimneySounds[m_iSoundIndex];
                //sound.clip = exhaustsounds;
                xAudioSourceComponent.Play();
            }

            m_iFramesSinceLastSpawn = 0;
        }

        const float fTimeDifference = 1.0f;
        m_xVelocity = fTimeDifference*(transform.position - m_xPrevPosition);
        m_xPrevPosition = transform.position;

        m_iFramesSinceLastSpawn++;
    }


    AudioClip[] LoadSoundsFromAssets(string path)
    {
        Object[] xGenericObjectsInDirectory = Resources.LoadAll(path);
        List<AudioClip> axSoundsInDirectory = new List<AudioClip>();

        for (int i = 0; i < xGenericObjectsInDirectory.Length; i++)
        {
            axSoundsInDirectory.Add((AudioClip)xGenericObjectsInDirectory[i]);
        }

        return axSoundsInDirectory.ToArray();
    }
}