using UnityEngine;
using System.Collections;

public class SmokeMover : MonoBehaviour {

    public float m_fTimeMultiplier = 15.0f;
    const float m_fColourMultiplier = 0.05f;
    const float m_fMaximumAge = 450.0f;
    const float m_fSmokeInitalUpVel = 0.1f;

    public Vector3 m_xSmoke = new Vector3(0.0f, 0.0f, 0.0f);
    public Vector3 initialSmokeVel;
    public float m_fAge;
    private static int s_iColourPropertyID;

    // Use this for initialization
    void Start () 
	{
		m_fAge = 1.0f; // Set to 1 why?
		initialSmokeVel = new Vector3( 0.0f, m_fSmokeInitalUpVel, 0.0f );
		m_xSmoke += initialSmokeVel;
        // This work happens each time one of these is made, which is not great.
        s_iColourPropertyID = Shader.PropertyToID("_Color"); 
    }
		
	// Update is called once per frame
	void FixedUpdate () 
	{
		if( m_fAge > m_fMaximumAge )
		{
			DestroyObject( transform.gameObject );
            return;
		}
		
        // Scale this object in time. (This is unnecessarily expensive)
		float tsqrt = Mathf.Sqrt(0.2f*m_fAge);
		transform.localScale = new Vector3 (tsqrt, tsqrt, 1.0f);

        // Translate the smoke particle.
        transform.localPosition = transform.localPosition + m_xSmoke;

        // Rotate it to face the camera
        transform.rotation = Camera.main.transform.rotation;

        //vSmoke.y = upSmokeVel*Mathf.Exp(-t*decayConst);
        m_xSmoke = 0.99f*m_xSmoke;

        // Update the colour
        float fColour = m_fAge / m_fMaximumAge;
        fColour = Mathf.Min(255, fColour);

        Color xColour = new Color(fColour, fColour, fColour);
        transform.GetComponent<MeshRenderer>().material.SetColor(s_iColourPropertyID, xColour);

        // Age this object.
        m_fAge = m_fAge + m_fTimeMultiplier*Time.deltaTime;
	}
}
