using UnityEngine;
using System.Collections;

public class GearPointUpdater : MonoBehaviour {
	
    public class GearRodConstraint
	{
        // The distance between 
		private float m_fDistanceSquared;
		private Vector3 m_xInitialPoint;
		
		public GearRodConstraint( Vector3 xInitialPos, Vector3 xInitialLocalPos, Vector3 xSpec )
		{
			m_xInitialPoint = xInitialLocalPos;
			//using .sqrMagnitude instead of squaring the distance below
			m_fDistanceSquared = (xSpec - xInitialPos).sqrMagnitude; 
		}
		
		public Vector3 GetRodEndpoint( Vector3 xSpec )
		{
			// Take the vector pointing from the initial point to the vector given in the argument
			Vector3 xDiff = (xSpec - m_xInitialPoint);
            // The float m_fDistanceSquared is fixed during construction.
            float fzTemp = -Mathf.Sqrt(m_fDistanceSquared - xDiff.x*xDiff.x - xDiff.y*xDiff.y) + xSpec.z;
			
			if(m_fDistanceSquared - xDiff.x*xDiff.x - xDiff.y*xDiff.y < 0)
			{
				print("WARNING: Unepected negative variable!");
			}

            // Return a vector which is fixed on the plane x = m_xInitialPoint.x
            return new Vector3(m_xInitialPoint.x, xSpec.y, fzTemp);
		}
	}

    // At the moment we aim to model three parts of the gear, given the labels A, B and C. 
    // Refer to a Walschaerts gear image.. e.g. https://en.wikipedia.org/wiki/Walschaerts_valve_gear#Technical_details
    // This is odd, why are we modelling only a few parts?
    public GameObject WheelParent;
    // A_1, A_2, B_1 are marker points on the rods.
    public GameObject A_1; 
    public GameObject A_2;
    public GameObject B_1;
    public GameObject Rod_A; // Rod_A is the 'Radius rod'
    public GameObject Rod_B; // Rod_B is the 'Combination lever'
    public GameObject Rod_C; // Rod_C is the 'Valve spindle
    // What is this? It is used to set a few initial variables and update them.
    // We only ever use it for Rod_A, the 'Radius rod'
    public GearRodConstraint Constraint;
	public float fSomeFloat = 0;
	private Vector3 xScaleVectorFromTransform;
	private Transform xWheelParentTransform;
	
	
	// Use this for initialization
	void Start () 
	{	
		xWheelParentTransform = WheelParent.transform;
		
		Constraint = new GearRodConstraint(A_2.transform.position, A_2.transform.localPosition, A_1.transform.position);
		
		// The scale of the wheels should be constant after initisalisation, the wheels won't scale
		// during gameplay except for if we're debugging.
		xScaleVectorFromTransform.x = 1f/xWheelParentTransform.localScale.x;
		xScaleVectorFromTransform.y = 1f/xWheelParentTransform.localScale.y;
		xScaleVectorFromTransform.z = 1f/xWheelParentTransform.localScale.z;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		Quaternion xInverseRotation;
		// Prepare inverse rotation
		xInverseRotation = xWheelParentTransform.rotation;
		xInverseRotation = Quaternion.Inverse(xInverseRotation);
		
		// Find local positions for the end points from their global positions.
		Vector3 A_1_translated = A_1.transform.position - xWheelParentTransform.position;
		A_1_translated = xInverseRotation*A_1_translated;
		A_1_translated = Vector3.Scale(A_1_translated,xScaleVectorFromTransform);
		
		Vector3 B_1_translated = B_1.transform.position - xWheelParentTransform.position;
		B_1_translated = xInverseRotation*B_1_translated;
		B_1_translated = Vector3.Scale(B_1_translated,xScaleVectorFromTransform);
		
		// Find the local position the endpoint A_2 given the position of A_1 in local space
		A_2.transform.localPosition = Constraint.GetRodEndpoint(A_1_translated);
		
		// Rotate Rods A and B about A_1_translated and B_1_translated respectively to point at A_2 
		
		Rod_A.transform.localPosition = 0.5f*(A_2.transform.localPosition + A_1_translated);
		Rod_A.transform.localRotation = GetRotationFromVector(A_2.transform.localPosition - A_1_translated);

		Rod_B.transform.localPosition = 0.5f*(A_2.transform.localPosition + B_1_translated);
		Rod_B.transform.localRotation = GetRotationFromVector(A_2.transform.localPosition - B_1_translated);
		
		// Find the position of Rod C along Rod B
		Vector3 xTempVector = A_2.transform.localPosition - B_1_translated;
		float fTempFloat = xTempVector.z/xTempVector.x;
		Rod_C.transform.localPosition = new Vector3(-fSomeFloat + B_1_translated.x,Rod_B.transform.localPosition.y,-fSomeFloat*fTempFloat + B_1_translated.z);
	}
	
	
	public Quaternion GetRotationFromVector( Vector3 xDirectionVector )
	{
		xDirectionVector.Normalize();
		
		float fAngle = Mathf.Atan2(xDirectionVector.z,xDirectionVector.x);
		
		Quaternion xRotation = Quaternion.Euler(0,-(180/Mathf.PI)*fAngle + 90, 0);
		
		return xRotation;
	}
}