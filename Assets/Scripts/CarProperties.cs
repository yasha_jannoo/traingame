using UnityEngine;

public class CarProperties : MonoBehaviour
{
    // t is parameter of tracks, v is velocity
    // angle stores the direction the car points in (really should store orientation instead)
    public Vector3 m_xWorldSpacePosition;
    public Vector3 m_xVelocity; 
    public float angle; 
    public float m_fAttatchPointDistance;
    public float t;
    public float m_fForwardThrust;
    public Tracks m_xCurrentTrack;
    public int m_iCurrentTrackPieceIndex;
    public Quaternion rotation;

    public void Initialise(float tee, Vector3 vee)
    {
        t = tee;
        m_xVelocity = vee;
    }

    void Start()
    {
        m_fAttatchPointDistance = 20.0f;
    }
}
