using UnityEngine;
using System.Collections;

public class RK4Solve : MonoBehaviour 
{

	// Effectively the acceleration
	private static float CalcFunction1(float a1, float b1)
	{
		return a1 - 0.5f*b1;
	}

	// Effectively the velocity
	private static float g1(float a1, float b1)
	{
		return b1;
	}

	// Iterate forwards, using input of x and v at time t, outputting x and v at time t + t*fDelta
	public static void PropagateForwards(ref float x, ref float v, ref float t, float zthrust)
	{
		// Remembering that this iterator combines a weighted average of four propagations,
		// here labelled Terms1-4, to find the next values.
		/*
		 * d2/dt2 x = ztrust - 0.5f * d/dt x
		 * 
		 * let
		 * d/dt x = v
		 * 
		 * gives
		 * 
		 * "x component" for the next v iteration
		 * d/dt v = zthrust - 0.5f * v = CalcFunction1(zthrust,v)
		 * "y component" for the next x iteration
		 * d/dt x = v = g1(zthrust, v)
		 * 
		 * must be solved simultaneously
		 * */
		
		Vector2 xTerm1;
		Vector2 xTerm2;
		Vector2 xTerm3;
		Vector2 xTerm4;
		// Hopefully this is creating value types from the references passed in
		float fInputPos = x;
		float fInputTime = v;
		float fInputVel = t;
		const float fDelta = 0.05f;

		// TODO: Is xTerm1.y correct? These quantities have different dimensions.. 
		xTerm1.x = CalcFunction1( zthrust, fInputVel );
		xTerm1.y = v + xTerm1.x;
		xTerm2.x = CalcFunction1( zthrust, fInputVel + fDelta*0.5f*xTerm1.y );
		xTerm2.y = v + fDelta*0.5f*xTerm2.x;
		xTerm3.x = CalcFunction1( zthrust, fInputVel + fDelta*0.5f*xTerm2.y );
		xTerm3.y = v + fDelta*0.5f*xTerm3.x;
		xTerm4.x = CalcFunction1( zthrust, fInputVel + fDelta*xTerm3.y );
		xTerm4.y = v + fDelta*xTerm3.x;
		
		v = fInputVel + (fDelta/6.0f)*(xTerm1.x + 2.0f*xTerm2.x + 2.0f*xTerm3.x + xTerm4.x);
		x = fInputPos + (fDelta/6.0f)*(xTerm1.y + 2.0f*xTerm2.y + 2.0f*xTerm3.y + xTerm4.y);
		
		t = fInputTime + fDelta;	
	}

}
