﻿using UnityEngine;
using UnityEngine.Assertions;

public class GunHandler : MonoBehaviour
{

    [HideInInspector]
    public enum eTurningConstraint
    {
        GLOBAL_UP_PERPENDICULAR_TO_FWDS,
        GLOBAL_UP_NOT_FIXED,
        GLOBAL_FIXED_UP_VECTOR,
        GLOBAL_UP_PARALLEL_TO_Y_AXIS,
        GLOBAL_FIXED_RIGHT_VECTOR,
        FIXED_UP_VECTOR_METHOD1,
        FIXED_UP_VECTOR_METHOD2,
    }

    public GameObject m_xTarget;
    public static bool s_bShowDebugLines = true;
    public float m_fBulletCalibre = 0.2f;
    public eTurningConstraint m_eConstraintType;
    private bool m_bIsFiring = true;
    private GameObject m_xBullet;
    private Vector3 m_xOriginalForward;
    private Vector3 m_xOriginalUp;

    // Use this for initialization
    void Start()
    {
        m_xOriginalUp = transform.up;
        m_xOriginalForward = transform.forward;
        // Create a bullet in future. For now just adopt the one that exists as a child to this.
        // Not fussed about doing checks for this.
        m_xBullet = transform.FindChild("Bullet").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_xTarget == null)
        {
            return;
        }

        // Target relative vector.
        Vector3 xDesiredFwd = m_xTarget.transform.position - transform.position;

        // Change this and the condition later on.
        if (xDesiredFwd == m_xOriginalForward)
        {
            return;
        }

        TurnToFaceTarget(xDesiredFwd, m_eConstraintType);

        // Move the bullet forwards by half the distance between the gun and target, then scale it in the y axis.
        float fTargetDistance = xDesiredFwd.magnitude;


        // Don't bother drawing a bullet if one has not been assigned
        if (m_xBullet == null)
        {
            return;
        }

        if (m_bIsFiring && (Time.frameCount % 30 == 0))
        {
            m_xBullet.transform.localPosition = new Vector3(0.0f, 0.0f, 0.5f * fTargetDistance);
            m_xBullet.transform.localScale = new Vector3(m_fBulletCalibre, fTargetDistance, m_fBulletCalibre);
        }
        else
        {
            m_xBullet.transform.localPosition = Vector3.zero;
            m_xBullet.transform.localScale = Vector3.zero;
        }
    }

    // Use this to turn the turrent to follow a target. Must provide a turning constraint.
    // Some of the GLOBAL_ prefixed rotations require additional testing.
    void TurnToFaceTarget(Vector3 xTargetForward, eTurningConstraint eConstraint)
    {
        // Almost certain I can simplify this algebraically/I am complicating this.
        Vector3 xDesiredRight = Vector3.Cross(xTargetForward, new Vector3(xTargetForward.x, 0.0f, xTargetForward.z));

        switch (eConstraint)
        {
            case eTurningConstraint.GLOBAL_UP_PERPENDICULAR_TO_FWDS:
                {
                    // The following is a rotation trying to keep up fixed perpendicular to forwards. 
                    transform.localRotation = Quaternion.LookRotation(xTargetForward, Vector3.Cross(xDesiredRight, xTargetForward));
                    break;
                }
            case eTurningConstraint.GLOBAL_FIXED_UP_VECTOR:
                {
                    // The following is a rotation trying to keep up fixed along the initial y axis.
                    // The forward vector must always lie in the plane given by the original up normal.
                    Vector3 xPlanarVector = m_xOriginalUp.normalized;
                    xPlanarVector = Vector3.Dot(xPlanarVector, xTargetForward) * xPlanarVector;
                    xPlanarVector = xTargetForward - xPlanarVector;
                    transform.localRotation = Quaternion.LookRotation(xPlanarVector, m_xOriginalUp);
                    break;
                }
            case eTurningConstraint.GLOBAL_FIXED_RIGHT_VECTOR:
                {
                    // The following is a rotation trying to keep up fixed perpendicular to forwards and the right vector fixed. 
                    Vector3 xFixedForward = m_xOriginalForward;
                    xFixedForward.y = xTargetForward.y;
                    transform.localRotation = Quaternion.LookRotation(xFixedForward, Vector3.Cross(xDesiredRight, xFixedForward));
                    break;
                }
            case eTurningConstraint.FIXED_UP_VECTOR_METHOD1:
                {
                    if (xTargetForward.Equals(Vector3.zero))
                    {
                        break;
                    }

                    // Take world space up direction
                    Vector3 xPlanarVector = transform.up;
                    // Dot with target vector relative to transform.position
                    xPlanarVector = Vector3.Dot(xPlanarVector, xTargetForward) * xPlanarVector;
                    // Find vector in-plane
                    xPlanarVector = xTargetForward - xPlanarVector;

                    if(s_bShowDebugLines)
                    {
                        Debug.DrawLine(transform.position, transform.position + xPlanarVector);
                    }

                    // WORLD SPACE Create rotation from vector.
                    transform.rotation = Quaternion.LookRotation(xPlanarVector, transform.up);
                    break;
                }
            case eTurningConstraint.FIXED_UP_VECTOR_METHOD2:
                {
                    if (xTargetForward.Equals(Vector3.zero))
                    {
                        break;
                    }

                    // METHOD 2
                    // Transform vector into local space.
                    Vector3 xAnotherPlanarVector = transform.InverseTransformVector(xTargetForward);
                    // Set Y component to zero.
                    xAnotherPlanarVector.y = 0.0f;
                    // Transform back to world space
                    xAnotherPlanarVector = transform.TransformVector(xAnotherPlanarVector);

                    if (s_bShowDebugLines)
                    {
                        Debug.DrawLine(transform.position, transform.position + xAnotherPlanarVector, Color.red);
                    }

                    // LOCAL SPACE Create rotation from vector.
                    //transform.localRotation = Quaternion.LookRotation(xAnotherPlanarVector, new Vector3(0.0f, 1.0f, 0.0f));
                    // WORLD SPACE Create rotation from vector.
                    transform.rotation = Quaternion.LookRotation(xAnotherPlanarVector, transform.up);
                    break;
                }
            default:
                {
                    Assert.IsTrue(false, "Constraint must be used when rotating in the gun handler");
                    return;
                }
        }
    }
}

// Two more options for later consideration.....
// The following is a rotation not keeping up fixed
//transform.rotation = Quaternion.FromToRotation(m_xOriginalForward, xCurrentPos);
// The following is an in-plane rotation
//transform.rotation = Quaternion.LookRotation(new Vector3(xDesiredFwd.x, 0.0f, xDesiredFwd.z));