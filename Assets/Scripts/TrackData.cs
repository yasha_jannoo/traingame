using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class TrackData : MonoBehaviour
{
	
	//  Each track piece can have a different length with a lower bound of "10". The entire length of track
	//  is now the thing that needs to be parameterised by "t". This means that t needs to be normalised
	//  with the total length of track. Each track has an associated ID. Joints are characterised by a reference 
	//  to the joining track
	public List<Tracks> m_xTrackList;
    public string m_szSave = "Tracks";
    public string m_szLoad = "Tracks";
    private string m_szDefault = "Tracks";
	
	// Use this for initialization
	void Start () 
	{
		m_xTrackList = new List<Tracks>();
		
		m_xTrackList.Add( new Tracks(Vector3.zero,0) );
		m_xTrackList[0].m_axTrackConnectingPoint.Add( Vector3.zero );
		
		m_xTrackList[0].CalculateTotalTrackLength();


        // Actually, load a default set of tracks.
        LoadTracks();
	}

    public void ClearTracks()
    {
        m_xTrackList.Clear();
        // Refresh the track meshes
        (transform.GetComponent<TrackEditing>()).m_bRecalcMesh = true;
    }

    public void SaveTracks()
	{
        if(m_szSave.Length == 0)
        {
            Debug.LogError("No save file name provided, tracks won't be saved.");
        }

		BinaryFormatter xFormatter = new BinaryFormatter();
		FileStream xFile = File.Create( Application.persistentDataPath + "/" + m_szSave.Split('.')[0] + ".tg");
		
		// Create a list of serialised track objects
		List<TracksSerialized> xSaveTracks = new List<TracksSerialized>(m_xTrackList.Count);
		
		// Loop over each track we have and add it to the serialised track list 
		for( int u = 0; u < m_xTrackList.Count; u++)
		{
			TracksSerialized xTrack = new TracksSerialized();
			
			for( int w = 0; w < m_xTrackList[u].m_axTrackConnectingPoint.Count; w++ )
			{
				xTrack.m_afTrackConnectingPointX.Add( m_xTrackList[u].m_axTrackConnectingPoint[w].x );
				xTrack.m_afTrackConnectingPointY.Add( m_xTrackList[u].m_axTrackConnectingPoint[w].y );
				xTrack.m_afTrackConnectingPointZ.Add( m_xTrackList[u].m_axTrackConnectingPoint[w].z );
			}
			
				xTrack.m_iTrackID = m_xTrackList[u].m_iTrackID;
				xTrack.edit_point = m_xTrackList[u].edit_point;
				xTrack.m_fTotalLength = m_xTrackList[u].m_fTotalLength;
				xTrack.m_fLength = m_xTrackList[u].m_fLength;
				xTrack.joiningTracks = m_xTrackList[u].joiningTracks;
			
			xSaveTracks.Add(xTrack);
		}
		
		xFormatter.Serialize( xFile, xSaveTracks );
		xFile.Close();
		print("Tracks saved");
	}
	
	public void LoadTracks()
	{
        string szTrackPath = Application.persistentDataPath + "/" + (m_szLoad.Length == 0 ? m_szDefault : m_szLoad) + ".tg";

        if ( !File.Exists(szTrackPath) )
		{
            Debug.LogError("Could not find tracks file " + szTrackPath + " to load from");
			return;
		}
		
		m_xTrackList.Clear();
		BinaryFormatter xFormatter = new BinaryFormatter();
		FileStream xFile = File.OpenRead(szTrackPath);
		
		List<TracksSerialized> xSavedTrackList = (List<TracksSerialized>)xFormatter.Deserialize( xFile );
		
		for( int u = 0; u < xSavedTrackList.Count; u++)
		{
			// Create a tracks object with some dummy variables
			Tracks xTrack = new Tracks( Vector3.zero, 0 );
			// Clear the first dummy saved track..
			xTrack.m_axTrackConnectingPoint.Clear();
			
			for( int w = 0; w < xSavedTrackList[u].m_afTrackConnectingPointX.Count; w++ )
			{
				Vector3 xVector = new Vector3();
				xVector.x = xSavedTrackList[u].m_afTrackConnectingPointX[w];
				xVector.y = xSavedTrackList[u].m_afTrackConnectingPointY[w];
				xVector.z = xSavedTrackList[u].m_afTrackConnectingPointZ[w];
				
				xTrack.m_axTrackConnectingPoint.Add( xVector );
			}
			
				xTrack.m_iTrackID = xSavedTrackList[u].m_iTrackID;
				xTrack.edit_point = xSavedTrackList[u].edit_point;
				xTrack.m_fTotalLength = xSavedTrackList[u].m_fTotalLength;
				xTrack.m_fLength = xSavedTrackList[u].m_fLength;
				xTrack.joiningTracks = xSavedTrackList[u].joiningTracks;
			
			m_xTrackList.Add(xTrack);
		}
		
		xFile.Close();
		print("Tracks loaded");
		
		// Refresh the track meshes
		(transform.GetComponent<TrackEditing>()).m_bRecalcMesh = true;
	}
	
	
}

// Ideally smaller and more concise than the full class "Tracks"
[System.Serializable]
public class TracksSerialized
{
	public List<float> m_afTrackConnectingPointX;
	public List<float> m_afTrackConnectingPointY;
	public List<float> m_afTrackConnectingPointZ;
	public int m_iTrackID;
	public int edit_point;
	public float m_fTotalLength;
	public List<float> m_fLength;
	public List<Joint> joiningTracks;
	
	public TracksSerialized()
	{
		this.m_afTrackConnectingPointX = new List<float>();
		this.m_afTrackConnectingPointY = new List<float>();
		this.m_afTrackConnectingPointZ = new List<float>();
		this.joiningTracks = new List<Joint>();
	}
}

public class Joint
{
    // joiners ident tells us which object is joined to this track. Possible
    // objects are enumerated. Each intereted object type from this class sets
    // the type. This is read at serialisation/deserialisation.
    public enum JointType
    {
        TRACK,
        TOWN
    }

    JointType m_eJointObjectType;

    // A joint occurs at a given point along the track.
    int joinPoint;

    public Joint(int zjoinPoint, JointType eType)
    {
        joinPoint = zjoinPoint;
        m_eJointObjectType = eType;
    }
}

public class TrackJoint : Joint
{
    Tracks m_xJoinTrack;
    public TrackJoint(Tracks zjoinTrack, int zjoinPoint) : base(zjoinPoint, JointType.TRACK)
    {
        m_xJoinTrack = zjoinTrack;
    }
}

public class TownJoint : Joint
{
    Town m_xJoinTown;
    public TownJoint(Town xTown, int zjoinPoint) : base(zjoinPoint, JointType.TOWN)
    {
        m_xJoinTown = xTown;
    }
}

public class Tracks
{
	public List<Vector3> m_axTrackConnectingPoint = new List<Vector3>();
    // Not really useful yet but this may be in future, though C# might work well with references.
	public int m_iTrackID;
	public int edit_point = 0;
	public float m_fTotalLength = 0f;
	public List<float> m_fLength = new List<float>();
	public List<Joint> joiningTracks = new List<Joint>();
	
	public Tracks(Vector3 start, int indentity)
	{
		m_axTrackConnectingPoint.Add(start);
		m_iTrackID = indentity;
	}

	
	
	public void CalculateTotalTrackLength()
	{
		// method finds total length for normalisation
		// (27/01/16 - why are recalculating the length at each point? This all needs a rethink at some point)
		m_fLength.Clear();
		m_fTotalLength = 0f;
		m_fLength.Add(0.0f);
		
		for(int i = 0; i < m_axTrackConnectingPoint.Count - 1; i++)
		{
			float fDifference = (m_axTrackConnectingPoint[i+1]-m_axTrackConnectingPoint[i]).magnitude;
			m_fLength.Add(m_fLength[i] + fDifference);
			m_fTotalLength += fDifference; //atm not correct
		}
	}
	
	public void AddIntersection(Tracks jointrack, int a)
	{
		// method adds a new joining track
		joiningTracks.Add(new TrackJoint(jointrack,a)); //I wonder if I can do something with properties
	}

    public void AddTown(Town xTown, int a)
    {
        // method adds a new joining track
        joiningTracks.Add(new TownJoint(xTown, a)); //I wonder if I can do something with properties
    }
}