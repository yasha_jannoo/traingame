﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour {

    static string m_sObjective = "Reach the next town";
    static GameObject m_xObjectiveText;

    // Use this for initialization
	void Start ()
    {
        m_xObjectiveText = GameObject.Find("ObjectiveText");
        Assert.IsNotNull(m_xObjectiveText, "Objective text object not found");

        if (m_xObjectiveText != null)
        {
            m_xObjectiveText.GetComponent<Text>().text = m_sObjective;
        }

        // Starts deactivated
        m_xObjectiveText.SetActive(false);
    }

    // Displays objective text on-screen by toggling.
    public void ToggleObjective()
    {
        m_xObjectiveText.SetActive(!m_xObjectiveText.activeSelf);
    }
}
